#!/usr/bin/env python

import setuptools
from setuptools import setup

setup(name='Distutils',
      version='0.1',
      description='Wait for the time you need',
      author='Alessandro Tufi',
      scripts=['lethargy']
)
